package com.jyoti.blogpost.service;

import com.jyoti.blogpost.entity.PostEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PostServiceImpl implements PostService{

    @Autowired
    private MongoTemplate mongoTemplate;

    public List<PostEntity> getAllPosts(){
        return mongoTemplate.findAll(PostEntity.class);
    }

    public PostEntity createPost(PostEntity postEntity){
        return mongoTemplate.save(postEntity);
    }

}
