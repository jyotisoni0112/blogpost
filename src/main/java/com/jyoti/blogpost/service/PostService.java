package com.jyoti.blogpost.service;

import com.jyoti.blogpost.entity.PostEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PostService {

    public List<PostEntity> getAllPosts();

    public PostEntity createPost(PostEntity postEntity);
}
