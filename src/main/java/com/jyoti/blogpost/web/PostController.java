package com.jyoti.blogpost.web;

import com.jyoti.blogpost.entity.PostEntity;
import com.jyoti.blogpost.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PostController {

    @Autowired
    private PostService postService;

    @GetMapping("/post")
    public List<PostEntity> getAllPost(){
        List<PostEntity> allPost = postService.getAllPosts();
        return allPost;
    }

    @PostMapping("/create-post")
    public PostEntity createPost(@RequestBody PostEntity postEntity){
        PostEntity postCreated = postService.createPost(postEntity);
        return postCreated;
    }
}
